FROM node:22-alpine AS builder

WORKDIR /app

RUN apk add --no-cache git jq

COPY package.json ./
RUN npm install $(jq -r '.dependencies | to_entries | map("\(.key)@\(.value)") | join(" ")' package.json) --global

ENTRYPOINT ["/bin/sh"]